import binascii
import os

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


@python_2_unicode_compatible
class Custom16Token(models.Model):
    """
    Custom authorization token model based on django-rest-framework/rest_framework/authtoken. 
    It was made because we needed to have an 16 characters token system.
    """
    key = models.CharField(max_length=40, primary_key=True)
    user = models.OneToOneField(AUTH_USER_MODEL, related_name='custom_token')
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Custom16Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(8)).decode()

    def __str__(self):
        return self.user.username
