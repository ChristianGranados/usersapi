from .models import Custom16Token
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken


class ObtainAuthCustom16Token(ObtainAuthToken):

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Custom16Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

obtain_auth_custom16token = ObtainAuthCustom16Token.as_view()
