#!/usr/bin/env python
# encoding: utf-8
"""
.. module:: users_to_mysql.py
   :platform: OS X Yosemite
   :synopsis: Module for data migration from CSV to MySQL.
.. moduleauthor: Christian Granados <christian@merideangroup.com>
.. date: 16/08/2015

"""

import MySQLdb
from datetime import datetime

# Opens database connection.
mydb = MySQLdb.connect(host='localhost',
                       user='root',
                       passwd='',
                       db='userapi')

# Prepares a cursor object using cursor() method.
cursor = mydb.cursor()

inputFile = "csv_backend.csv"

with open(inputFile, 'r') as fr:
    data = fr.readlines()

    for line in data:
        words = line.strip().split(",")
        username = words[0]
        email = words[1]
        date_joined = datetime.strptime(words[2], '%Y-%m-%d %H:%M:%S.%f')
        last_login = datetime.strptime(words[3], '%Y-%m-%d %H:%M:%S.%f')

        # Prepares SQL query to INSERT a record into the database.
        sql = """"INSERT INTO `platzi_users_platziuser` (username,
                  email, date_joined, last_login) VALUES (%s, %s, %s, %s)"""
        try:
            # Executes the SQL command
            cursor.execute(sql, (username, email, date_joined, last_login))
            # Commits the changes in the database.
            mydb.commit()
        except:
            # Rollback in case there is any error.
            mydb.rollback()
    # Disconnects from server.
    mydb.close()
fr.close()
