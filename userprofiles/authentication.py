from rest_framework.authentication import *
from tokens.models import Custom16Token


class CustomTokenAuthentication(TokenAuthentication):
    """
    Returns 401 in case the API KEY is invalid and it is
    based in custom16tokens model.
    """
    model = Custom16Token

    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        msg = 'APIKeyinvalid.'

        if not auth or auth[0].lower() != b'token':
            raise exceptions.AuthenticationFailed(msg)

        if len(auth) == 1:
            raise exceptions.AuthenticationFailed(msg)

        elif len(auth) > 2:
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(auth[1])

    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.select_related('user').get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed(('Invalid token.'))

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (token.user, token)
