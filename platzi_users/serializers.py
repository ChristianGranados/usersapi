from rest_framework import serializers


class PlatziUserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=30)
    email = serializers.CharField(max_length=100)
    date_joined = serializers.DateTimeField()
    last_login = serializers.DateTimeField()
