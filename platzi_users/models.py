from django.db import models


class PlatziUser(models.Model):
    username = models.CharField(max_length=30, blank=False)
    email = models.CharField(max_length=100, blank=False)
    date_joined = models.DateTimeField(blank=False)
    last_login = models.DateTimeField()
