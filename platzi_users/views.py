from rest_framework import viewsets
from userprofiles import authentication

from .models import PlatziUser
from .serializers import PlatziUserSerializer


class PlatziUserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PlatziUserSerializer
    queryset = PlatziUser.objects.all()
    authentication_classes = (authentication.CustomTokenAuthentication,)
