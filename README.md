# Christian Granados UsersApp

# Requirements

* Python (2.7)
* Django (1.8)
* MySQL Server version: 5.6.23 MySQL Community Server (GPL)

# Structure:
	This proyect is composed with two sides:
		A) Django Restful Project (UsersAPI)
		B) Python program for importing data from CVS to MySQLDB (this file is located at /utils/users_to_mysql.py)

# Installation

1) Install dependencies using `pip` withing a virtual environment.

    pip install -r requirements.txt

2) Make sure that database parameters correspond to yours within the file usersapi/settings.py: 

	DATABASES = {
	    'default': {
	        'ENGINE': 'django.db.backends.mysql',
	        'NAME': 'userapi',
	        'USER': 'yourdbuser',
	        'PASSWORD': 'yourdbuserpassword',
	        'HOST': 'localhost',
    	}
	}

3) Run the next command to install models:
	python manage.py migrate

# CSV TO MYSQL PROGRAM

- Adjust the next parameters within the file with your MySQL configuration and put the cvs file in the same
directory where users_to_mysql.py file is located:

	mydb = MySQLdb.connect(host='localhost',
	                       user='root',
	                       passwd='',
	                       db='userapi')

- You can run this program by simply running: python /utils/users_to_mysql.py

4) Create an admin user

5) Create an user for the email system using the admin app.

6) Check the API KEY for the email system user in the Custom16 tokens model within Admin dashboard so you can test with it.

7) Run development server in localhost so you can test the API:
	127.0.0.1:8000/users
